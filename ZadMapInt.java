import java.util.*;


public class ZadMapInt {
    public static void main(String[] args) {
        Map<Double, Double> pow = new LinkedHashMap<>();
        for (double i = -10; i <=10 ; i += .5) {
            pow.put(i, i * i);
        }
        pow.entrySet().stream().
                forEach(a-> System.out.printf("%7.2f \t %7.2f %n", a.getKey(), a.getValue()));
    }

}
